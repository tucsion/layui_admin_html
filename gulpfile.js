var gulp = require('gulp')
var connect = require('gulp-connect')

var ROOT_PATH = './src'

gulp.task('connect', function () {
    connect.server({
        root: 'src',
        livereload: true,
    })
})

gulp.task('watch', function () {
    gulp.watch([ROOT_PATH + '/*.html', ROOT_PATH + '/**/*.css', ROOT_PATH + '/**/*.js', ROOT_PATH + '/images/'], function () {
        gulp.src(ROOT_PATH + '/*.html')
            .pipe(gulp.dest(ROOT_PATH))
            .pipe(connect.reload())
    })
})

gulp.task('default', ['connect', 'watch'], function () {
    console.log('启动了gulp任务')
})
